# redis-operator

![Version: 0.16.2](https://img.shields.io/badge/Version-0.16.2-informational?style=flat-square) ![AppVersion: 0.17.0](https://img.shields.io/badge/AppVersion-0.17.0-informational?style=flat-square)

Provides easy redis setup definitions for Kubernetes services, and deployment.

**Homepage:** <https://github.com/OT-CONTAINER-KIT/redis-operator>

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| iamabhishek-dubey |  |  |
| sandy724 |  |  |
| shubham-cmyk |  |  |

## Source Code

* <https://github.com/OT-CONTAINER-KIT/redis-operator>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| certificate.name | string | `"serving-cert"` |  |
| certificate.secretName | string | `"webhook-server-cert"` |  |
| issuer.email | string | `"shubham.gupta@opstree.com"` |  |
| issuer.name | string | `"redis-operator-issuer"` |  |
| issuer.privateKeySecretName | string | `"letsencrypt-prod"` |  |
| issuer.server | string | `"https://acme-v02.api.letsencrypt.org/directory"` |  |
| issuer.solver.enabled | bool | `true` |  |
| issuer.solver.ingressClass | string | `"nginx"` |  |
| issuer.type | string | `"selfSigned"` |  |
| nodeSelector | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| priorityClassName | string | `""` |  |
| redisOperator.env | list | `[]` |  |
| redisOperator.extraArgs | list | `[]` |  |
| redisOperator.imageName | string | `"ghcr.io/ot-container-kit/redis-operator/redis-operator"` |  |
| redisOperator.imagePullPolicy | string | `"Always"` |  |
| redisOperator.imageTag | string | `""` |  |
| redisOperator.name | string | `"redis-operator"` |  |
| redisOperator.podAnnotations | object | `{}` |  |
| redisOperator.podLabels | object | `{}` |  |
| redisOperator.watch_namespace | string | `""` |  |
| redisOperator.webhook | bool | `false` |  |
| replicas | int | `1` |  |
| resources.limits.cpu | string | `"500m"` |  |
| resources.limits.memory | string | `"500Mi"` |  |
| resources.requests.cpu | string | `"500m"` |  |
| resources.requests.memory | string | `"500Mi"` |  |
| securityContext | object | `{}` |  |
| service.name | string | `"webhook-service"` |  |
| service.namespace | string | `"redis-operator"` |  |
| serviceAccountName | string | `"redis-operator"` |  |
| tolerateAllTaints | bool | `false` |  |
| tolerations | list | `[]` |  |
